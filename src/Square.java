/**
 * Created by Jone on 1/10/2016.
 */
public enum Square {
	EMPTY('.'),
	MINE('*'),
	NONE('0'),
	ONE('1'),
	TWO('2'),
	THREE('3'),
	FOUR('4'),
	FIVE('5'),
	SIX('6'),
	SEVEN('7'),
	EIGHT('8');


	private char value;

	Square(char value)
	{
		this.value = value;
		Singleton.lookup.put(value, this);
	}

	public char getSquareValue()
	{
		return this.value;
	}


	// look up the Square from hash map by using the corresponding string
	public static Square getSquare(char value)
	{
		return Singleton.lookup.get(value);
	}
}
