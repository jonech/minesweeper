/**
 * Created by Jone on 1/10/2016.
 */
public class Minesweeper {

	private int row;
	private int col;

	private Square[][] board;

	public Minesweeper(int row, int col) {
		this.row = row;
		this.col = col;
		board = new Square[row][col];
	}


	/**
	 * Takes a 2D char array board and clone it into a Square board
	 * @param board
	 */
	public void CloneBoard(char[][] board)
	{

		for (int i=0; i<row; i++)
			for (int j=0; j<col; j++) {
				this.board[i][j] = Square.getSquare(board[i][j]);
			}


	}

	/**
	 * Calculate the mines surrounding a specific square in 8 directions
	 * @param rowPos the row position
	 * @param colPos the column position
	 * @return
	 */
	public int CountMines(int rowPos, int colPos) {
		int total = 0;

		// cache the directions
		int[][] directions =
				{{-1, 1}, {0, 1}, {1, 1},
						{-1, 0},          {1, 0},
						{-1, -1}, {0, -1}, {1, -1}};

		// sum up the total mines in 8 directions
		for (int d=0; d<directions.length; d++) {

			int temp_row = rowPos + directions[d][0];
			int temp_col = colPos + directions[d][1];

			// make sure that the direction is within array bound
			if (temp_col >= 0 && temp_col < col && temp_row >= 0 && temp_row < row)
				if (board[temp_row][temp_col] == Square.MINE)
					total ++;

		}
		return total;
	}


	/**
	 * Fill up the board with the numbers of mines around the empty squares
	 */
	public void SuperSweep()
	{
		for (int i=0; i<row; i++)
			for (int j=0; j<col; j++)
				if (board[i][j] == Square.EMPTY)
					board[i][j] = Square.getSquare(Integer.toString(CountMines(i, j)).charAt(0));

	}

	public char[][] getBoard()
	{
		char[][] temp = new char[row][col];
		for (int i=0; i<row; i++)
			for (int j=0; j<col; j++) {
				temp[i][j] = board[i][j].getSquareValue();
			}

		return temp;
	}

	/**
	 * Print the board
	 */
	public void printBoard() {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(board[i][j].getSquareValue());
			}
			System.out.println("");
		}

	}
}
