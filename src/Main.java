/**
 * Created by Jone on 1/10/2016.
 */
public class Main {

	public static char[][] board1 = {{'*', '.', '.', '.'},
									{'*', '.', '*', '.'},
									{'*', '.', '.', '.'},
									{'*', '*', '.', '.'}};

	public static void main(String args[]) {

		int row = 4;
		int col = 4;

		// initialize board
		Minesweeper ms = new Minesweeper(row, col);


		ms.CloneBoard(board1);

		// start filling the board
		ms.SuperSweep();
		// output
		ms.printBoard();
	}
}
